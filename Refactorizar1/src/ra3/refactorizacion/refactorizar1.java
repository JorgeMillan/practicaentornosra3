package ra3.refactorizacion;

import java.util.Scanner;

public class refactorizar1 {															//cambiar el nombre de la clase a refactorizar1
	
	final static String cad = "Bienvenido al programa";
	
	public static void main(String[] args) {		
		int a;
		int b;																			//a y b declarados en distinta linea	
		String cadena1;
		String cadena2;																	//cadena1 y cadena2 declaradas en distinta linea
		Scanner c = new Scanner(System.in);
		System.out.println(cad);
		System.out.println("Introduce tu dni");
		cadena1 = c.nextLine();
		System.out.println("Introduce tu nombre");
		cadena2 = c.nextLine();
		c.close();																		//cerrar el scanner c
		
		a=7; b=16;
		int numeroc = 25;	
		if ( (a > b || numeroc%5 != 0) && ((numeroc*3-1) > b / numeroc)) {				//llave "{" bien puesta, distintos espacios y paréntesis en el if
			System.out.println("Se cumple la condición");
		}	
		
		numeroc = (a + b) * (numeroc + b / a);											//espacios y paréntesis en numeroc	
		String[] array = new String[7];													//poner los "[]" al lado de String
		array[0] = "Lunes";
		array[1] = "Martes";
		array[2] = "Miercoles";
		array[3] = "Jueves";
		array[4] = "Viernes";
		array[5] = "Sabado";
		array[6] = "Domingo";

		recorrer_array(array);
	}
	
	static void recorrer_array(String vectordestrings[]) {								//llave "{" bien puesta
		
		for(int dia = 0; dia < 7 ;dia++) {												//distintos espacios en el for para que se vea mejor, llave "{" bien puesta	
			System.out.println("El dia de la semana en el que te encuentras "			//syso de +100 caracteres, en dos lineas,
					+ "["+ (dia + 1) +"-7] es el dia: "+vectordestrings[dia]);			//espacios en la operacion del syso
		}
	}
	
}