package principal;

import java.util.Scanner;

public class Generador {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int longitud =0;
		int opcion=0;
		longitud = menu(in);
		opcion = password(in);
		String password = "";
		switch (opcion) {
		case 1:
			password = alfabeto(longitud, password);
			break;
		case 2:
			password = numeros(longitud, password);
			break;
		case 3:
			password = letraConCaracteresEspeciales(longitud, password);
			break;
		case 4:
			password = numeroLetraConCaracterEspecial(longitud, password);
			break;
		}
		System.out.println(password);
		in.close();
	}

	private static String numeroLetraConCaracterEspecial(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int n= (int) (Math.random() * 3);
			if (n == 1) {
				char letra4=(char) ((Math.random() * 26) + 65);;
				password += letra4;
			} else if (n == 2) {
				char caracter4= (char) ((Math.random() * 15) + 33);;
				password += caracter4;
			} else {
				int numero4= (int) (Math.random() * 10);;
				password += numero4;
			}
		}
		return password;
	}

	private static String letraConCaracteresEspeciales(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int n =(int) (Math.random() * 2);;
			if (n == 1) {
				char letra3= (char) ((Math.random() * 26) + 65);
					password += letra3;
			} else {
				char caracter3= (char) ((Math.random() * 15) + 33);
				
				password += caracter3;
			}
		}
		return password;
	}

	private static String numeros(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int numero2= (int) (Math.random() * 10);
			
			password += numero2;
		}
		return password;
	}

	private static String alfabeto(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			char letra1= (char) ((Math.random() * 26) + 65);
			password += letra1;
		}
		return password;
	}

	private static int password(Scanner in) {
		int opcion=0;
		System.out.println("Elige tipo de password: ");
		 opcion = in.nextInt();
		return opcion;
	}

	private static int menu(Scanner in) {
		int longitud=0;
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
		System.out.println("Introduce la longitud de la cadena: ");
		 longitud = in.nextInt();
		return longitud;
	}
}