package ra3.refactorizacion;													//poner el package

import java.util.Scanner;														//importar solo lo necesario

public class refactorizar2 {

	static Scanner a;
	public static void main(String[] args) {									//poner bien "{"
		a = new Scanner(System.in);	
		int n;																	//n y cantidad declarados en lineas separadas
																				//cantidad_maxima_alumnos no se usaba, eliminado
		int[] arrays = new int[10];												//poner los "[]" al lado de int
		for(n = 0; n < 10; n++) {												//espacios en el for, poner bien "{"		
			System.out.println("Introduce nota media de alumno");
			arrays[n] = a.nextInt();
		}	
		
		System.out.println("El resultado es: " + recorrer_array(arrays));	
		a.close();
		
	}
	static double recorrer_array(int vector[]) {								//poner bien "{"
		double c = 0;
		for(int a = 0; a < 10; a++) {											//espacios en el for, poner bien "{"
			c += vector[a];														//cambiado   c=c+vector[a]   por   c += vector[a];
		}
		double d = c/10;														//declarado c/10 por una variable
		return d;
	}
}